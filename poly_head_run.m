%%
% "A Sketching Framework for Reduced Data Transfer in Photon Counting Lidar"
%
% Michael P. Sheehan, Julian Tachella, Mike E. Davies
%
% A preprint can be found in https://arxiv.org/abs/2102.08732.
% Contact: michael.sheehan@ed.ac.uk    julian.tachella@ed.ac.uk
%
% The dataset is from the single photon group at 
% Heriot-Watt University (https://single-photon.com)
%
% The function pcshow requires the Computer Vision Toolbox
clc;clear all; close all;

%% Load Dataset
load('head40m_100ms.mat')
dimensions=size(Y);
width=dimensions(1);
height=dimensions(2);
T=dimensions(3);

%% Aligning IRF
h=h(:);
h1=zeros(T,1);
h1(1:length(h))=h;
h=h1;
[mx,idx]=max(h);
h=circshift(h,-idx+1); 
h=h/sum(h);

%% set sketch parameters
m=2;
disp(['Reconstructing the data using ' num2str(m) ' sketches per pixel' ])
depth_smle=cell(height,width);
intensity_smle=cell(height,width);
%% Scan the lidar scene pixel-by-pixel
for ii=1:height
disp(['Processing row ' num2str(ii)])
    for jj=1:width
    [~,X]=Poiss2Mix_data(Y,[ii,jj]);

    if ~isempty(X)
        sketch=sketch_wrapper(X,m,T);
        sketch.compute_sketch(X);
        K=1;
        estimator=lidar_estimator(sketch,h);
        estimator.init_params(K);
        estimator.tol=1e-2;
        estimator.estim(K);
        t_smle=estimator.t_est;
        alpha_smle=estimator.alpha_est;
        n=estimator.n;
        depth_smle{ii,jj}=t_smle;
        intensity_smle{ii,jj}=n*alpha_smle;
    end

    end
end

output_smle_depth=reshape(depth_smle,[height*width,1]);
output_smle_int=reshape(intensity_smle,[height*width,1]);
scale_ratio=7;
bin_width=0.3;
[p_smle] = results2pcloud(output_smle_depth,output_smle_int,scale_ratio,bin_width,width);
figure(1)
pcshow(p_smle)
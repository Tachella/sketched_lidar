# Sketched lidar package

Papers associated with this code:

1. "A Sketching Framework for Reduced Data Transfer in Photon Counting Lidar" IEEE Trans. on Computational Imaging

2. "Surface Detection for Sketched Single Photon Lidar", EUSIPCO 2021

Papers: https://ieeexplore.ieee.org/document/9541047, https://arxiv.org/abs/2105.06920

Blogpost: https://tachella.github.io/2021/02/20/a-sketching-framework-for-reduced-data-transfer-in-photon-counting-lidar/

Sketch Lidar Package. Authors: Michael P. Sheehan, J. Tachella, M. E. Davies

© Copyright, University of Edinburgh 2021. All Rights Reserved.

## Requirements

* MATLAB (tested on 2019b)
* Computer Vision Toolbox (optional for 3D plotting)


## Testing the package

We provide a simple script to try the sketching algorithm for synthetic lidar data:
```
test_sketch_lidar.m
```

The reconstruction of a full real lidar dataset can be tested with 
```
poly_head_run.m
```

The detection algorithm from sketches can be tested with 
```
sketch_detection_run.m
```


## Summary of results
The proposed method can achieve reconstruct with a very small number of sketches without significant loss of depth resolution, whereas other methods, such as coarse binning the time-of-flight histograms incur in significant loss of information.
<div align="center">
<img src="https://tachella.github.io/wp-content/uploads/2021/02/compressivelidar2-1414x1536.png"  width="512">
</div>

classdef lidar_detection<handle

properties

%output
decision;
stat;
T_stat;

%input
sketch_obj;
sig_level;
z_n;
n;
m;
end

methods

function self=lidar_detection(sketch_obj)
 self.sketch_obj=sketch_obj;
 self.m=self.sketch_obj.m;
 self.n=self.sketch_obj.n;
 self.z_n=self.sketch_obj.z_n;
end




function self=sketch_detect(self,sig_level)
  self.sig_level=sig_level;
  self.T_stat=self.n*(self.z_n'*self.z_n);
  threshold=chi2inv(1-self.sig_level,length(self.z_n));
  self.stat=self.T_stat-threshold;
  self.decision=0;
  if self.stat>0
      self.decision=1;
  end
end



end


end
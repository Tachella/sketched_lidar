function [y,X]=Poiss2Mix_data(Y,idx)
y=Y(idx(1),idx(2),:);
y=y(:);
X = zeros(1,sum(y));
x = y;
k = 1; i =1;
while k<=size(X,2)
   
    while(y(i)==0)
        i=i+1;
    end
    
    X(k)= i;
    y(i) = y(i)-1;
    k = k+1;
end

y=Y(idx(1),idx(2),:);
y=y(:);
end
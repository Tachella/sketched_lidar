%%
% "Surface Detection for Sketched Single Photon Lidar"
%
% Michael P. Sheehan, Julian Tachella, Mike E. Davies
%
% IEEE EUSIPCO Conference
% Contact: michael.sheehan@ed.ac.uk    julian.tachella@ed.ac.uk
%
% The dataset is from the single photon group at 
% Heriot-Watt University (https://single-photon.com)

clc;clear all; close all;
%% Load Dataset
load('Detection Data\image_325m_F500_R50_1219hrs_30ms.mat')
%load('Detection Data\image_325m_F500_R50_1219hrs_3ms.mat')
Y=Y(:,:,10:2700);
dimensions=size(Y);
width=dimensions(1);
height=dimensions(2);
T=dimensions(3);

%% Aligning IRF
h=h(:);
h=circshift(h,-960);
[mx,idx]=max(h);
h1=zeros(T,1);
h1(1:length(h))=h;
h=h1;
[mx,idx]=max(h);
h=circshift(h,-idx+1); 
h=h/sum(h);
h_fft=conj(fft(h));

%% set sketch parameters
m=5;
Z_MD=zeros(height,width);
T_MD=zeros(height,width);
sig_level=0.05; %the significance level of the hypothesis test


%% Compute Data-Driven Background (See Section III.E)
Y_background= Y(1:15,1:180,:);
y = sum(sum(Y_background,2),1);
[~,X_background]=Poiss2Mix_data(y,[1,1]);
[z_n,w]=emp_sketch(X_background,m,size(Y,3)); 
mean_zn = z_n;

%% Scan the lidar scene pixel-by-pixel
for ii=1:height
disp(ii)
    for jj=1:width
[y,X]=Poiss2Mix_data(Y,[ii,jj]);

if length(X)>1
%hist(X,T)
sketch=sketch_wrapper(X,m,T);
sketch.compute_sketch(X);
sketch.z_n=sketch.z_n-mean_zn; %non-constant background correction

detector=lidar_detection(sketch);
detector.sketch_detect(sig_level);
T_MD(ii,jj)=detector.decision;
Z_MD(ii,jj)=detector.stat;
else

end


    end
end



%% Spatial Regularization (requires unlocbox Toolbox - see https://epfl-lts2.github.io/unlocbox-html/)
reg_tv=3; %Total Variation Reg parameter
Z_tv = tv_denoising(-Z_MD, reg_tv);
T_tv = (Z_tv<0);


%% Compare with Ground Truth
load('Detection Data\detection_ground_truth')
figure;
subplot(131)
imagesc(T_gt)
axis image
axis off
title('ground truth')

% Sketch

T=T_MD;
T = T>0.1;
T=T(:,1:180);
subplot(132)
imagesc(T)
axis image
axis off
title('Proposed')

disp(['Prob. of Detection: ' num2str(100*sum(T(T_gt==1))/sum(T_gt(:))) '%'])
disp(['Prob. of False Alarm: ' num2str(100-100*sum(T_gt(T==0)==0)/sum(T_gt(:)==0)) '%'])


% Sketch + Spatial Reg
T=T_tv;
T = T>0.1;
T=T(:,1:180);
subplot(133)
imagesc(T)
axis image
axis off
title('Proposed + TV')
disp(['Prob. of Detection: ' num2str(100*sum(T(T_gt==1))/sum(T_gt(:))) '%'])
disp(['Prob. of False Alarm: ' num2str(100-100*sum(T_gt(T==0)==0)/sum(T_gt(:)==0)) '%'])

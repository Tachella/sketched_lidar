classdef lidar_estimator<handle

properties

%output
t_est;
alpha_est;

%input
h;
h_fft;
sketch_obj;
m;
w;
z_n;
T;
n;

%defaults
tol;
t_init;
alpha_init;
t_step;
alpha_step;
max_iter;



end

methods

function self=lidar_estimator(sketch_obj,h)
self.tol=1e-3;

self.max_iter=500;
self.sketch_obj=sketch_obj;
self.m=self.sketch_obj.m;
self.T=self.sketch_obj.T;
self.n=self.sketch_obj.n;
self.t_step=100;
self.alpha_step=0.01;
self.z_n=self.sketch_obj.z_n;
self.w=self.sketch_obj.w;
self.h=h;
self.h_fft=conj(fft(self.h));
end

function init_params(self,K)
if K==1
%[self.t_init,self.alpha_init]=self.param_init(); %comment out for preferred K=1
%initialisation
[self.t_init,self.alpha_init]=self.circ_mean();
elseif K==2
[self.t_init,self.alpha_init]=self.param_init2D();
else 
disp('ERROR: Current version only supports K<=2')
end

end



function estim(self,K)

if K==1

[self.t_est,self.alpha_est]=self.sketched_lidar1D();
elseif K==2
[self.t_est,self.alpha_est]=self.sketched_lidar2D();
else 
disp('ERROR: Current version only supports K<=2')
end

end


% K=1 functions
function [t_est,alpha_est]=sketched_lidar1D(self)

t_tmp=self.t_init; alpha_tmp=self.alpha_init;
z1=self.char_func1D(alpha_tmp,t_tmp);
stop=0;it=0;
while stop==0
  it=it+1;
  G_t=self.SMLE_grad1D(t_tmp,alpha_tmp,z1,'loc'); %gradient w.r.t t
  t0_new=real(t_tmp-self.t_step*G_t);
  z1=self.char_func1D(alpha_tmp,t0_new);
  
 G_alpha=self.SMLE_grad1D(t0_new,alpha_tmp,z1,'prop'); %gradient w.r.t alpha
 alpha_new=real(alpha_tmp-self.alpha_step*G_alpha);
 if alpha_new<0.01
     alpha_new=0.01;
 end
 if alpha_new>=1
     alpha_new=0.999; %for stability especially for large m
 end
 z1=self.char_func1D(alpha_new,t0_new);
 mu_diff=norm(t_tmp-t0_new);
 alpha_diff=norm(alpha_tmp-alpha_new);
 t_tmp=t0_new;
 alpha_tmp=(alpha_new);
    
 if it>=self.max_iter
      stop=1;
 end
 error_tmp=mu_diff+alpha_diff;
  if (error_tmp<=self.tol)
      stop=1;
  end
end


t_est=(t0_new); alpha_est=alpha_new;
t_est=rem(t_est,self.T);
if t_est<0
    t_est=t_est+self.T;
end

 if alpha_est>=1
     alpha_est=0.999;
 end

end
function cf = char_func1D(self,alpha,t)

w_int=round(self.w*self.T/(2*pi)+1);
cf=zeros(self.m,1);
for j=1:self.m
   cf(j)=alpha*(self.h_fft(w_int(j)))*exp(1i*(self.w(j))*t);
end
end
function G = SMLE_grad1D(self,t,alpha,z,grad_type) 

g=zeros(self.m,1);
 Sig = self.CF_cov1D(alpha,t);
 
while cond(Sig)>1e6
    Sig=Sig+0.01*eye(self.m);  %added for stability
end

if strcmp(grad_type,'loc')
  
    for j=1:self.m
        w_j=self.w(j);
        idx1=round(w_j*self.T/(2*pi)+1);
        g(j)=1i*alpha*w_j*self.h_fft(idx1)*exp(1i*w_j*t);
   end

elseif strcmp(grad_type,'prop') 

    for j=1:self.m
       w_j=self.w(j);
       idx1=round(w_j*self.T/(2*pi)+1);
       g(j)=self.h_fft(idx1)*exp(1i*w_j*t);  
    end 
    
else
    print('error')
end
G=real(2*g'*(Sig\(z-self.z_n)));
end
function Sigma = CF_cov1D(self,alpha,t)

    Sigma=zeros(self.m,self.m);
    
    for l=1:self.m
        for k=l:self.m
            % set the frequencies
            w_kl=self.w(k)-self.w(l);
            w_k=self.w(k);
            w_l=self.w(l);
            
            % set the frequency index for h_fft
            idx1=round(w_kl*self.T/(2*pi)+1);
            idx2=round(w_k*self.T/(2*pi)+1);
            idx3=round(w_l*self.T/(2*pi)+1); 
            
            if idx1==0
                keyboard
            end
            
            % compute each indivual char. function
            cf_kl=alpha*self.h_fft(idx1)*exp(1i*w_kl*t);
            if idx1==1
                cf_kl=1;
            end
            cf_k=alpha*self.h_fft(idx2)*exp(1i*w_k*t);
            cf_l=alpha*conj(self.h_fft(idx3)*exp(1i*w_l*t));
            
            Sigma(k,l)=cf_kl-cf_k*cf_l;
            Sigma(l,k) = conj(Sigma(k,l));
        end 
       
            
    end


end
function [t0_init,alpha_init]=param_init(self)


grid_t0=linspace(0,self.T,100);
grid_alpha=linspace(0.5,1,25);


t0_init=0;alpha_init=0;
t0_loss=Inf;
for i=1:length(grid_alpha)
    for j=1:length(grid_t0)
 t0_tmp=grid_t0(j);
 alpha_tmp=grid_alpha(i);
 z=self.char_func1D(alpha_tmp,t0_tmp);
 t0_loss_tmp=real((z-self.z_n)'*(z-self.z_n));
 if t0_loss_tmp<t0_loss
     t0_loss=t0_loss_tmp;
     t0_init=t0_tmp;
     alpha_init=alpha_tmp;
 end  
    end
end
end
function [t0_cm,alpha_cm]=circ_mean(self)
asym_correction = angle(self.h_fft(2))*self.T/(2*pi);
t0_cm=(self.T/(2*pi))*atan2(imag(self.z_n(1)),real(self.z_n(1)));
t0_cm=t0_cm-asym_correction; %this corrects the bias of a asymmetric IRF
if t0_cm<0
    t0_cm=t0_cm+self.T;
end

 alpha_cm=rem(real((1/self.h_fft(2))*exp(-1i*self.w(1)*t0_cm)*self.z_n(1)),1);
 if alpha_cm<0
     alpha_cm=alpha_cm+1;
 end
 

end


% K=2 Functions
function [t_est,alpha_est]=sketched_lidar2D(self)

t_tmp=self.t_init; alpha_tmp=self.alpha_init;
z1=self.char_func2D(alpha_tmp,t_tmp);







stop=0;it=0;
while stop==0
  it=it+1;
  G_t1=self.SMLE_grad2D(t_tmp,alpha_tmp,z1,1,'loc');
  G_t2=self.SMLE_grad2D(t_tmp,alpha_tmp,z1,2,'loc');
  G=[G_t1,G_t2];
  t_new=t_tmp - self.t_step*G;

  z1=self.char_func2D(alpha_tmp,t_new);
  G_alpha1=self.SMLE_grad2D(t_new,alpha_tmp,z1,1,'prop');
  G_alpha2=self.SMLE_grad2D(t_new,alpha_tmp,z1,2,'prop');
  G_alpha=[G_alpha1,G_alpha2,0];
  alpha_new=alpha_tmp-self.alpha_step*G_alpha;
    
  
  
  
  for ll=1:2
 if alpha_new(ll)<0
     alpha_new(ll)=0;
 end
 if alpha_new(ll)>=0.99
     alpha_new(ll)=0.99;
 end
  end

 
 z1=self.char_func2D(alpha_new,t_new);
 mu_diff=norm(t_tmp-t_new);
 alpha_diff=norm(alpha_tmp-alpha_new);
 t_tmp=t_new;
 alpha_tmp=(alpha_new);
    
 if it>=self.max_iter;
      stop=1;
 end
 error_tmp=mu_diff+alpha_diff;
  if (error_tmp<=self.tol)
      stop=1;
  end
end


t_est=(t_new);
for ll=1:2
t_est(ll)=rem(t_est(ll),self.T);
if t_est(ll)<0
    t_est(ll)=t_est(ll)+self.T;
end
end
alpha_new(3)=max(0,1-sum(alpha_new(1:2)))
alpha_est=alpha_new/sum(alpha_new);  %normalise to sum to 1

end
function cf = char_func2D(self,alpha,t)
w_int=mod(fix((self.w)*self.T/(2*pi)+1),self.T);
for ww=1:self.m
    if w_int(ww)==0; w_int(ww)=self.T;end
end

cf=zeros(self.m,1);
a1=alpha(1);
a2=alpha(2);
t1=t(1);
t2=t(2);

for j=1:self.m
    cf(j)= (a1*(self.h_fft(w_int(j)))*exp(1i*(self.w(j))*t1))+(a2*(self.h_fft(w_int(j)))*exp(1i*(self.w(j))*t2)); 
end
end
function G = SMLE_grad2D(self,t,alpha,z,pos,grad_type) 

g=zeros(self.m,1);
Sig =self.CF_cov2D(alpha,t);

if strcmp(grad_type,'loc')
  
    for j=1:self.m
        w_j=self.w(j);
        idx=round(w_j*self.T/(2*pi)+1);
        g(j)=1i*alpha(pos)*w_j*self.h_fft(idx)*exp(1i*w_j*t(pos));
        
    end

elseif strcmp(grad_type,'prop')
    for j=1:self.m
       w_j=self.w(j);
       idx1=round(w_j*self.T/(2*pi)+1);
       g(j)=self.h_fft(idx1)*exp(1i*w_j*t(pos));   
    end 
    
else
    print('error')
end

G=real(2*g'*(Sig\(z-self.z_n)));
end
function Sigma = CF_cov2D(self,alpha,t)

    Sigma=zeros(self.m,self.m);
    
    for kk=1:self.m
        for ll=1:self.m
            % set the frequencies
            w_kl=self.w(kk)-self.w(ll);
            idx1=mod(fix((w_kl)*self.T/(2*pi)+1),self.T); if idx1==0; idx1=self.T;end
            w_k=self.w(kk);
            idx2=mod(fix((w_k)*self.T/(2*pi)+1),self.T);  if idx2==0; idx2=self.T;end
            w_l=-self.w(ll);
            idx3=mod(fix((w_l)*self.T/(2*pi)+1),self.T);  if idx3==0; idx3=self.T;end
            
        
            
            % compute each indivual char. function
            cf_kl=alpha(1)*self.h_fft(idx1)*exp(1i*(w_kl)*t(1))+alpha(2)*self.h_fft(idx1)*exp(1i*(w_kl)*t(2));
            if idx1==1
                cf_kl=1;
            end
            cf_k=alpha(1)*self.h_fft(idx2)*exp(1i*(w_k)*t(1))+alpha(2)*self.h_fft(idx2)*exp(1i*(w_k)*t(2));
            cf_l=alpha(1)*conj(self.h_fft(idx3))*exp(1i*(w_l)*t(1))+alpha(2)*conj(self.h_fft(idx3))*exp(1i*(w_l)*t(2));
            
            Sigma(kk,ll)=cf_kl-cf_k*cf_l;
          
        end 
       
            
    end


end
function [t1_init,alpha_init]=param_init2D(self)

grid_alpha=linspace(0.05,0.5,20);
grid_t1=linspace(0,self.T,10);
grid_t2=linspace(0,self.T,10);



t1_init=[0,0];
alpha_init=[0,0,0];
t0_loss=Inf;
for k=1:length(grid_alpha)
for i=1:length(grid_t1)
    for j=i+1:length(grid_t2)
 t0_tmp=[grid_t1(i),grid_t2(j)];
 alpha_tmp=[grid_alpha(k),1-grid_alpha(k),0];
 z=self.char_func2D(alpha_tmp,t0_tmp);
 t0_loss_tmp=real((z-self.z_n)'*(z-self.z_n));
 if t0_loss_tmp<t0_loss
     t0_loss=t0_loss_tmp;
     t1_init=t0_tmp;
    alpha_init=alpha_tmp;
 end  
    end
end
end
end




end







end
function [h,h_fun] = get_impulse(T,sigma)


    
h = exp(-((((1:T)'-T/2)/sigma).^2)/2)/(sqrt(2*pi)*sigma);


d = zeros(T,1);
d(1:length(h))=h;
d(length(h):end) = h(end);
h = d;
h = h/sum(h);
[~,attack] = max(h);
h = circshift(h,-attack+1);

% for i=1:length(h)
%     if h(i)<0.0001
%         h(i)=0;
%     end
% end

syms  t;
assume(t,'real'); assumeAlso(t>0)
h_fun = symfun(exp(-((t/sigma).^2)/2)/(sqrt(2*pi)*sigma),t);

end
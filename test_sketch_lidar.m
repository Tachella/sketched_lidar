%%
% "A Sketching Framework for Reduced Data Transfer in Photon Counting Lidar"
%
% Michael P. Sheehan, Julian Tachella, Mike E. Davies
%
% A preprint can be found in https://arxiv.org/abs/2102.08732.
% Contact: michael.sheehan@ed.ac.uk    julian.tachella@ed.ac.uk
%
% A script simulation a lidar scene using a Gaussian IRF

clear all; clc;
%% K=1 single surface scene
% simulate lidar scene
T=1000;
sigma=0.01*T;
t0=randi(T);
n=100;
SBR=10;
[h,~]=get_impulse(T,sigma);
[X,~] = synthetic_lidar(h,t0,n,SBR);

% compute sketch
m=5;
sketch=sketch_wrapper(X,m,T);
sketch.compute_sketch(X);
K=1;
estimator=lidar_estimator(sketch,h);
estimator.init_params(K);
estimator.estim(K);
disp(['True Location: ' num2str(t0) ' SMLE Estimate: ' num2str(estimator.t_est) ' Error: ' num2str(norm(t0-estimator.t_est))])

%% K=2 multi-surface scene
% simulate lidar scene
T=1000;
sigma=0.01*T;
T;
t0=[randi(T/2),T/2+randi(T/2)];
n=1000;
SBR=1;
alpha=SBR/(SBR+1);
alpha=[alpha/2,alpha/2,1-alpha];
[h,h_fun]=get_impulse(T,sigma);
[X,~] = synthetic_lidar(h,t0,n,SBR);

% compute sketch
m=5;
sketch=sketch_wrapper(X,m,T);
sketch.compute_sketch(X);
K=2;
estimator=lidar_estimator(sketch,h);
estimator.init_params(K);
estimator.estim(K);
disp(['True Location: ' num2str(t0) ' SMLE Estimate: ' num2str(sort(estimator.t_est)) ' Error: ' num2str(norm(t0-sort(estimator.t_est)))])

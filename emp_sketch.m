function [z,w]=emp_sketch(X,m,T)


w=zeros(m,1);
for i=1:m
    w(i)=2*(i)*(pi/(T));
end
    

z=zeros(m,1);
for j=1:m
    z(j)=sum(exp(1i*w(j)*(X)))/length(X);
end


end
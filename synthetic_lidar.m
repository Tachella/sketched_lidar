function [X,x] = synthetic_lidar(h,t0,PPP,SBR)

T = length(h);



s = PPP/(1+1/SBR);
b = PPP-s;

signal = zeros(size(h));
for i=1:length(t0)
    signal = signal + circshift(h,t0(i)-1);
end
signal = signal*s/length(t0);

background = b/T;

y = poissrnd(signal+background);

X = zeros(1,sum(y));

x = y;
k = 1; i =1;
while k<=size(X,2)
   
    while(y(i)==0)
        i=i+1;
    end
    
    X(k)= i;
    y(i) = y(i)-1;
    k = k+1;
end



end
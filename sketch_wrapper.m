classdef sketch_wrapper<matlab.mixin.Copyable

properties


% sizes
m; %sketch dim
T; % acq. time window
n; % photon count

% frequencies
w; %frequencies

z_n; % empirical sketch

end 



methods



   function self=sketch_wrapper(X,m,T)
            if nargin>0
                
                % options
                
                self.m=m;
                self.T=T;
                self.n=length(X);
            end
   end

    function compute_sketch(self,X)
    [self.z_n,self.w]=emp_sketch(X,self.m,self.T);
    end






end


end


